/**
 * NetworkServiceBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package NetworkService;

public class NetworkServiceBindingSkeleton implements NetworkService.NetworkServiceInterface, org.apache.axis.wsdl.Skeleton {
    private NetworkService.NetworkServiceInterface impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Login"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Type"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getAuthToken", _params, new javax.xml.namespace.QName("", "token"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getAuthToken"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getAuthToken") == null) {
            _myOperations.put("getAuthToken", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getAuthToken")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceSearch"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "DeviceSearch"), NetworkDataTypes.DeviceSearch.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("searchDevice", _params, new javax.xml.namespace.QName("", "DeviceList"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "searchDevice"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("searchDevice") == null) {
            _myOperations.put("searchDevice", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("searchDevice")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getDeviceBasicInfo", _params, new javax.xml.namespace.QName("", "Device"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "DeviceBasicInfo"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getDeviceBasicInfo"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getDeviceBasicInfo") == null) {
            _myOperations.put("getDeviceBasicInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getDeviceBasicInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getDeviceInfo", _params, new javax.xml.namespace.QName("", "Device"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "DeviceInfo"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getDeviceInfo"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getDeviceInfo") == null) {
            _myOperations.put("getDeviceInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getDeviceInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceNameList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"), java.lang.String[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getDeviceInfoArray", _params, new javax.xml.namespace.QName("", "DeviceInfoList"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfDeviceInfo"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getDeviceInfoArray"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getDeviceInfoArray") == null) {
            _myOperations.put("getDeviceInfoArray", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getDeviceInfoArray")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "MAC"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getDeviceInfoFromNameMAC", _params, new javax.xml.namespace.QName("", "Device"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "DeviceInfo"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getDeviceInfoFromNameMAC"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getDeviceInfoFromNameMAC") == null) {
            _myOperations.put("getDeviceInfoFromNameMAC", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getDeviceInfoFromNameMAC")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
        };
        _oper = new org.apache.axis.description.OperationDesc("getMyDeviceInfo", _params, new javax.xml.namespace.QName("", "Device"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "DeviceInfo"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getMyDeviceInfo"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getMyDeviceInfo") == null) {
            _myOperations.put("getMyDeviceInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getMyDeviceInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Minutes"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getLastChangedDevices", _params, new javax.xml.namespace.QName("", "DeviceList"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getLastChangedDevices"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getLastChangedDevices") == null) {
            _myOperations.put("getLastChangedDevices", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getLastChangedDevices")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Device"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "DeviceInput"), NetworkDataTypes.DeviceInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Cards"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfInterfaceCards"), NetworkDataTypes.InterfaceCard[].class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Interfaces"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfBulkInterfaces"), NetworkDataTypes.BulkInterface[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("bulkInsert", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "bulkInsert"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("bulkInsert") == null) {
            _myOperations.put("bulkInsert", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("bulkInsert")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Device"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "DeviceInput"), NetworkDataTypes.DeviceInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Cards"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfInterfaceCards"), NetworkDataTypes.InterfaceCard[].class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Interfaces"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfBulkInterfacesAuto"), NetworkDataTypes.BulkInterfaceAuto[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("bulkInsertAuto", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "bulkInsertAuto"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("bulkInsertAuto") == null) {
            _myOperations.put("bulkInsertAuto", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("bulkInsertAuto")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("bulkRemove", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "bulkRemove"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("bulkRemove") == null) {
            _myOperations.put("bulkRemove", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("bulkRemove")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Device"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "DeviceInput"), NetworkDataTypes.DeviceInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceInsert", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceInsert"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceInsert") == null) {
            _myOperations.put("deviceInsert", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceInsert")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceCard"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "InterfaceCard"), NetworkDataTypes.InterfaceCard.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceAddCard", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceAddCard"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceAddCard") == null) {
            _myOperations.put("deviceAddCard", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceAddCard")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "BulkInterface"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "BulkInterface"), NetworkDataTypes.BulkInterface.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceAddBulkInterface", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceAddBulkInterface"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceAddBulkInterface") == null) {
            _myOperations.put("deviceAddBulkInterface", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceAddBulkInterface")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceRemove", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceRemove"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceRemove") == null) {
            _myOperations.put("deviceRemove", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceRemove")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "HardwareAddress"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceRemoveCard", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceRemoveCard"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceRemoveCard") == null) {
            _myOperations.put("deviceRemoveCard", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceRemoveCard")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceRemoveBulkInterface", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceRemoveBulkInterface"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceRemoveBulkInterface") == null) {
            _myOperations.put("deviceRemoveBulkInterface", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceRemoveBulkInterface")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "BulkInterface"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "BulkInterface"), NetworkDataTypes.BulkInterface.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "BulkMoveOptions"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "BulkMoveOptions"), NetworkDataTypes.BulkMoveOptions.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceMoveBulkInterface", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceMoveBulkInterface"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceMoveBulkInterface") == null) {
            _myOperations.put("deviceMoveBulkInterface", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceMoveBulkInterface")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceInput"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "DeviceInput"), NetworkDataTypes.DeviceInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceUpdate", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceUpdate"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceUpdate") == null) {
            _myOperations.put("deviceUpdate", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceUpdate")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "NewDeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceGlobalRename", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceGlobalRename"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceGlobalRename") == null) {
            _myOperations.put("deviceGlobalRename", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceGlobalRename")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"), java.lang.String[].class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "HCPFlag"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("setHCPResponse", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "setHCPResponse"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("setHCPResponse") == null) {
            _myOperations.put("setHCPResponse", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("setHCPResponse")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "IPv6Ready"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceUpdateIPv6Ready", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceUpdateIPv6Ready"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceUpdateIPv6Ready") == null) {
            _myOperations.put("deviceUpdateIPv6Ready", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceUpdateIPv6Ready")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ManagerLocked"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceUpdateManagerLock", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceUpdateManagerLock"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceUpdateManagerLock") == null) {
            _myOperations.put("deviceUpdateManagerLock", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceUpdateManagerLock")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Device"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Server"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ImagePath"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceSetBOOTPInfo", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceSetBOOTPInfo"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceSetBOOTPInfo") == null) {
            _myOperations.put("deviceSetBOOTPInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceSetBOOTPInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Device"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceRemoveBOOTPInfo", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceRemoveBOOTPInfo"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceRemoveBOOTPInfo") == null) {
            _myOperations.put("deviceRemoveBOOTPInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceRemoveBOOTPInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Device"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getBOOTPInfo", _params, new javax.xml.namespace.QName("", "BOOTPInfo"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "BOOTPInfo"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getBOOTPInfo"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getBOOTPInfo") == null) {
            _myOperations.put("getBOOTPInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getBOOTPInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getBulkInterfaceInfo", _params, new javax.xml.namespace.QName("", "BulkInterface"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "BulkInterface"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getBulkInterfaceInfo"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getBulkInterfaceInfo") == null) {
            _myOperations.put("getBulkInterfaceInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getBulkInterfaceInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Set"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Address"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("setInsertAddress", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "setInsertAddress"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("setInsertAddress") == null) {
            _myOperations.put("setInsertAddress", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("setInsertAddress")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Set"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Service"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("setInsertService", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "setInsertService"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("setInsertService") == null) {
            _myOperations.put("setInsertService", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("setInsertService")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Set"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Address"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("setDeleteAddress", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "setDeleteAddress"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("setDeleteAddress") == null) {
            _myOperations.put("setDeleteAddress", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("setDeleteAddress")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Set"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Service"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("setDeleteService", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "setDeleteService"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("setDeleteService") == null) {
            _myOperations.put("setDeleteService", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("setDeleteService")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "SetName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getSetInfo", _params, new javax.xml.namespace.QName("", "SetInfo"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "SetInfo"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getSetInfo"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getSetInfo") == null) {
            _myOperations.put("getSetInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getSetInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "SetID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getSetNameFromID", _params, new javax.xml.namespace.QName("", "SetName"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getSetNameFromID"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getSetNameFromID") == null) {
            _myOperations.put("getSetNameFromID", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getSetNameFromID")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "SetName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getSetAllInterfaces", _params, new javax.xml.namespace.QName("", "Interfaces"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getSetAllInterfaces"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getSetAllInterfaces") == null) {
            _myOperations.put("getSetAllInterfaces", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getSetAllInterfaces")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "SetName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getSetInterfacesTrusting", _params, new javax.xml.namespace.QName("", "Interfaces"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getSetInterfacesTrusting"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getSetInterfacesTrusting") == null) {
            _myOperations.put("getSetInterfacesTrusting", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getSetInterfacesTrusting")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Hosts"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"), java.lang.String[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getHCPInfoArray", _params, new javax.xml.namespace.QName("", "HostsInfo"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfInetInfo"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getHCPInfoArray"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getHCPInfoArray") == null) {
            _myOperations.put("getHCPInfoArray", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getHCPInfoArray")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Service"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getDevicesFromService", _params, new javax.xml.namespace.QName("", "Hosts"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getDevicesFromService"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getDevicesFromService") == null) {
            _myOperations.put("getDevicesFromService", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getDevicesFromService")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Service"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getSwitchesFromService", _params, new javax.xml.namespace.QName("", "Hosts"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getSwitchesFromService"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getSwitchesFromService") == null) {
            _myOperations.put("getSwitchesFromService", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getSwitchesFromService")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "SwitchName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getSwitchInfo", _params, new javax.xml.namespace.QName("", "SwitchInfo"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfSwitchPort"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getSwitchInfo"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getSwitchInfo") == null) {
            _myOperations.put("getSwitchInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getSwitchInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getConnectionsFromDevice", _params, new javax.xml.namespace.QName("", "ConnectionInfo"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfConnection"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getConnectionsFromDevice"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getConnectionsFromDevice") == null) {
            _myOperations.put("getConnectionsFromDevice", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getConnectionsFromDevice")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "SwitchName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "PortName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getOutletLocationFromSwitchPort", _params, new javax.xml.namespace.QName("", "OutletLocation"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "OutletLocation"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getOutletLocationFromSwitchPort"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getOutletLocationFromSwitchPort") == null) {
            _myOperations.put("getOutletLocationFromSwitchPort", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getOutletLocationFromSwitchPort")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ip"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "HardwareAddressList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"), java.lang.String[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getCurrentConnection", _params, new javax.xml.namespace.QName("", "ObservedConnections"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfObservedSwitchConnection"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getCurrentConnection"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getCurrentConnection") == null) {
            _myOperations.put("getCurrentConnection", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getCurrentConnection")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "HardwareAddressList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"), java.lang.String[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getMyCurrentConnection", _params, new javax.xml.namespace.QName("", "ObservedConnections"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfObservedSwitchConnection"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getMyCurrentConnection"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getMyCurrentConnection") == null) {
            _myOperations.put("getMyCurrentConnection", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getMyCurrentConnection")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "SwitchName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "PortName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("enableFanOutFromSwitchPort", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "enableFanOutFromSwitchPort"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("enableFanOutFromSwitchPort") == null) {
            _myOperations.put("enableFanOutFromSwitchPort", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("enableFanOutFromSwitchPort")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "HardwareAddress"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("bindUnbindInterface", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "bindUnbindInterface"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("bindUnbindInterface") == null) {
            _myOperations.put("bindUnbindInterface", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("bindUnbindInterface")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Alias"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("interfaceAddAlias", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "interfaceAddAlias"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("interfaceAddAlias") == null) {
            _myOperations.put("interfaceAddAlias", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("interfaceAddAlias")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Alias"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("interfaceRemoveAlias", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "interfaceRemoveAlias"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("interfaceRemoveAlias") == null) {
            _myOperations.put("interfaceRemoveAlias", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("interfaceRemoveAlias")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Alias"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "NewInterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("interfaceMoveAlias", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "interfaceMoveAlias"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("interfaceMoveAlias") == null) {
            _myOperations.put("interfaceMoveAlias", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("interfaceMoveAlias")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "NewInterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("interfaceRename", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "interfaceRename"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("interfaceRename") == null) {
            _myOperations.put("interfaceRename", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("interfaceRename")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "NewDeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("interfaceMove", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "interfaceMove"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("interfaceMove") == null) {
            _myOperations.put("interfaceMove", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("interfaceMove")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "SetPattern"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("searchSet", _params, new javax.xml.namespace.QName("", "Sets"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "searchSet"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("searchSet") == null) {
            _myOperations.put("searchSet", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("searchSet")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Set"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "SetInput"), NetworkDataTypes.SetInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("setInsert", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "setInsert"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("setInsert") == null) {
            _myOperations.put("setInsert", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("setInsert")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "SetName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("setRemove", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "setRemove"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("setRemove") == null) {
            _myOperations.put("setRemove", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("setRemove")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "SwitchName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "PortName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "SwitchPortTypeStatus"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "SwitchPortTypeStatus"), NetworkDataTypes.SwitchPortTypeStatus.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("setSwitchPortTypeStatus", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "setSwitchPortTypeStatus"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("setSwitchPortTypeStatus") == null) {
            _myOperations.put("setSwitchPortTypeStatus", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("setSwitchPortTypeStatus")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "SwitchName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "PortName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Service"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("setSwitchPortService", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "setSwitchPortService"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("setSwitchPortService") == null) {
            _myOperations.put("setSwitchPortService", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("setSwitchPortService")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "SwitchName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "PortName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getSwitchPortTypeStatus", _params, new javax.xml.namespace.QName("", "SwitchPortTypeStatus"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "SwitchPortTypeStatus"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getSwitchPortTypeStatus"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getSwitchPortTypeStatus") == null) {
            _myOperations.put("getSwitchPortTypeStatus", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getSwitchPortTypeStatus")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "NetName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("searchNetNameTable", _params, new javax.xml.namespace.QName("", "NetNameTable"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfNetNameTuple"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "searchNetNameTable"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("searchNetNameTable") == null) {
            _myOperations.put("searchNetNameTable", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("searchNetNameTable")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "LogicalInterface"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "LogicalInterfaceInput"), NetworkDataTypes.LogicalInterfaceInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceAddLogicalInterface", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceAddLogicalInterface"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceAddLogicalInterface") == null) {
            _myOperations.put("deviceAddLogicalInterface", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceAddLogicalInterface")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deviceRemoveLogicalInterface", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "deviceRemoveLogicalInterface"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deviceRemoveLogicalInterface") == null) {
            _myOperations.put("deviceRemoveLogicalInterface", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deviceRemoveLogicalInterface")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Description"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("interfaceUpdateDescription", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "interfaceUpdateDescription"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("interfaceUpdateDescription") == null) {
            _myOperations.put("interfaceUpdateDescription", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("interfaceUpdateDescription")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ServiceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Description"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("serviceUpdateDescription", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "serviceUpdateDescription"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("serviceUpdateDescription") == null) {
            _myOperations.put("serviceUpdateDescription", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("serviceUpdateDescription")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ServiceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getServiceInfo", _params, new javax.xml.namespace.QName("", "ServiceInfo"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ServiceInfo"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "getServiceInfo"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getServiceInfo") == null) {
            _myOperations.put("getServiceInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getServiceInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMDevice"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "DeviceInput"), NetworkDataTypes.DeviceInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMCreateOptions"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "VMCreateOptions"), NetworkDataTypes.VMCreateOptions.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmCreate", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmCreate"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmCreate") == null) {
            _myOperations.put("vmCreate", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmCreate")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "NewParent"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmMigrate", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmMigrate"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmMigrate") == null) {
            _myOperations.put("vmMigrate", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmMigrate")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceInput"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "DeviceInput"), NetworkDataTypes.DeviceInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmUpdate", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmUpdate"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmUpdate") == null) {
            _myOperations.put("vmUpdate", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmUpdate")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmDestroy", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmDestroy"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmDestroy") == null) {
            _myOperations.put("vmDestroy", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmDestroy")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMClusterName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmClusterGetInfo", _params, new javax.xml.namespace.QName("", "VMClusterInfo"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "VMClusterInfo"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmClusterGetInfo"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmClusterGetInfo") == null) {
            _myOperations.put("vmClusterGetInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmClusterGetInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMClusterName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmClusterGetDevices", _params, new javax.xml.namespace.QName("", "Hosts"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmClusterGetDevices"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmClusterGetDevices") == null) {
            _myOperations.put("vmClusterGetDevices", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmClusterGetDevices")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmGetInfo", _params, new javax.xml.namespace.QName("", "VMInfo"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "VMInfo"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmGetInfo"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmGetInfo") == null) {
            _myOperations.put("vmGetInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmGetInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DeviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmGetClusterMembership", _params, new javax.xml.namespace.QName("", "VMClusterList"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmGetClusterMembership"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmGetClusterMembership") == null) {
            _myOperations.put("vmGetClusterMembership", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmGetClusterMembership")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMClusterSearch"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "VMClusterSearch"), NetworkDataTypes.VMClusterSearch.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmSearchCluster", _params, new javax.xml.namespace.QName("", "VMClusterList"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfString"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmSearchCluster"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmSearchCluster") == null) {
            _myOperations.put("vmSearchCluster", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmSearchCluster")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMClusterName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMInterfaceOptions"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "VMInterfaceOptions"), NetworkDataTypes.VMInterfaceOptions.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmAddInterface", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmAddInterface"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmAddInterface") == null) {
            _myOperations.put("vmAddInterface", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmAddInterface")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmRemoveInterface", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmRemoveInterface"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmRemoveInterface") == null) {
            _myOperations.put("vmRemoveInterface", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmRemoveInterface")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMClusterName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMInterfaceOptions"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "VMInterfaceOptions"), NetworkDataTypes.VMInterfaceOptions.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmMoveInterface", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmMoveInterface"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmMoveInterface") == null) {
            _myOperations.put("vmMoveInterface", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmMoveInterface")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InterfaceCard"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "InterfaceCard"), NetworkDataTypes.InterfaceCard.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmAddCard", _params, new javax.xml.namespace.QName("", "HardwareAddress"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmAddCard"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmAddCard") == null) {
            _myOperations.put("vmAddCard", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmAddCard")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "VMName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "HardwareAddress"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("vmRemoveCard", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "vmRemoveCard"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("vmRemoveCard") == null) {
            _myOperations.put("vmRemoveCard", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("vmRemoveCard")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Zone"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DnsZoneOptions"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "DnsZoneOptions"), NetworkDataTypes.DnsZoneOptions.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("dnsZoneUpdate", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "dnsZoneUpdate"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("dnsZoneUpdate") == null) {
            _myOperations.put("dnsZoneUpdate", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("dnsZoneUpdate")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Search"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("dnsDelegatedSearch", _params, new javax.xml.namespace.QName("", "DNSDelegatedEntries"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfDNSDelegatedEntry"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "dnsDelegatedSearch"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("dnsDelegatedSearch") == null) {
            _myOperations.put("dnsDelegatedSearch", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("dnsDelegatedSearch")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Domain"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "View"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("dnsDelegatedGetByNameView", _params, new javax.xml.namespace.QName("", "DNSDelegatedEntry"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "DNSDelegatedEntry"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "dnsDelegatedGetByNameView"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("dnsDelegatedGetByNameView") == null) {
            _myOperations.put("dnsDelegatedGetByNameView", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("dnsDelegatedGetByNameView")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DNSDelegatedInput"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "DNSDelegatedInput"), NetworkDataTypes.DNSDelegatedInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("dnsDelegatedAdd", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "dnsDelegatedAdd"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("dnsDelegatedAdd") == null) {
            _myOperations.put("dnsDelegatedAdd", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("dnsDelegatedAdd")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("dnsDelegatedListKeys", _params, new javax.xml.namespace.QName("", "DNSDelegatedKeys"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:NetworkDataTypes", "ArrayOfDNSDelegatedKey"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "dnsDelegatedListKeys"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("dnsDelegatedListKeys") == null) {
            _myOperations.put("dnsDelegatedListKeys", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("dnsDelegatedListKeys")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Domain"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "View"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("dnsDelegatedRemove", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "dnsDelegatedRemove"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("dnsDelegatedRemove") == null) {
            _myOperations.put("dnsDelegatedRemove", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("dnsDelegatedRemove")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Domain"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "View"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Alias"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("dnsDelegatedAliasAdd", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "dnsDelegatedAliasAdd"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("dnsDelegatedAliasAdd") == null) {
            _myOperations.put("dnsDelegatedAliasAdd", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("dnsDelegatedAliasAdd")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:NetworkService", "Auth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:NetworkDataTypes", "Auth"), NetworkDataTypes.Auth.class, true, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Domain"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "View"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Alias"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("dnsDelegatedAliasRemove", _params, new javax.xml.namespace.QName("", "Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:NetworkService", "dnsDelegatedAliasRemove"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("dnsDelegatedAliasRemove") == null) {
            _myOperations.put("dnsDelegatedAliasRemove", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("dnsDelegatedAliasRemove")).add(_oper);
    }

    public NetworkServiceBindingSkeleton() {
        this.impl = new NetworkService.NetworkServiceBindingImpl();
    }

    public NetworkServiceBindingSkeleton(NetworkService.NetworkServiceInterface impl) {
        this.impl = impl;
    }
    public java.lang.String getAuthToken(java.lang.String login, java.lang.String password, java.lang.String type) throws java.rmi.RemoteException
    {
        java.lang.String ret = impl.getAuthToken(login, password, type);
        return ret;
    }

    public java.lang.String[] searchDevice(NetworkDataTypes.Auth auth, NetworkDataTypes.DeviceSearch deviceSearch) throws java.rmi.RemoteException
    {
        java.lang.String[] ret = impl.searchDevice(auth, deviceSearch);
        return ret;
    }

    public NetworkDataTypes.DeviceBasicInfo getDeviceBasicInfo(java.lang.String deviceName) throws java.rmi.RemoteException
    {
        NetworkDataTypes.DeviceBasicInfo ret = impl.getDeviceBasicInfo(deviceName);
        return ret;
    }

    public NetworkDataTypes.DeviceInfo getDeviceInfo(NetworkDataTypes.Auth auth, java.lang.String deviceName) throws java.rmi.RemoteException
    {
        NetworkDataTypes.DeviceInfo ret = impl.getDeviceInfo(auth, deviceName);
        return ret;
    }

    public NetworkDataTypes.DeviceInfo[] getDeviceInfoArray(NetworkDataTypes.Auth auth, java.lang.String[] deviceNameList) throws java.rmi.RemoteException
    {
        NetworkDataTypes.DeviceInfo[] ret = impl.getDeviceInfoArray(auth, deviceNameList);
        return ret;
    }

    public NetworkDataTypes.DeviceInfo getDeviceInfoFromNameMAC(java.lang.String deviceName, java.lang.String MAC) throws java.rmi.RemoteException
    {
        NetworkDataTypes.DeviceInfo ret = impl.getDeviceInfoFromNameMAC(deviceName, MAC);
        return ret;
    }

    public NetworkDataTypes.DeviceInfo getMyDeviceInfo() throws java.rmi.RemoteException
    {
        NetworkDataTypes.DeviceInfo ret = impl.getMyDeviceInfo();
        return ret;
    }

    public java.lang.String[] getLastChangedDevices(NetworkDataTypes.Auth auth, int minutes) throws java.rmi.RemoteException
    {
        java.lang.String[] ret = impl.getLastChangedDevices(auth, minutes);
        return ret;
    }

    public boolean bulkInsert(NetworkDataTypes.Auth auth, NetworkDataTypes.DeviceInput device, NetworkDataTypes.InterfaceCard[] cards, NetworkDataTypes.BulkInterface[] interfaces) throws java.rmi.RemoteException
    {
        boolean ret = impl.bulkInsert(auth, device, cards, interfaces);
        return ret;
    }

    public boolean bulkInsertAuto(NetworkDataTypes.DeviceInput device, NetworkDataTypes.InterfaceCard[] cards, NetworkDataTypes.BulkInterfaceAuto[] interfaces) throws java.rmi.RemoteException
    {
        boolean ret = impl.bulkInsertAuto(device, cards, interfaces);
        return ret;
    }

    public boolean bulkRemove(NetworkDataTypes.Auth auth, java.lang.String deviceName) throws java.rmi.RemoteException
    {
        boolean ret = impl.bulkRemove(auth, deviceName);
        return ret;
    }

    public boolean deviceInsert(NetworkDataTypes.Auth auth, NetworkDataTypes.DeviceInput device) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceInsert(auth, device);
        return ret;
    }

    public boolean deviceAddCard(NetworkDataTypes.Auth auth, java.lang.String deviceName, NetworkDataTypes.InterfaceCard interfaceCard) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceAddCard(auth, deviceName, interfaceCard);
        return ret;
    }

    public boolean deviceAddBulkInterface(NetworkDataTypes.Auth auth, java.lang.String deviceName, NetworkDataTypes.BulkInterface bulkInterface) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceAddBulkInterface(auth, deviceName, bulkInterface);
        return ret;
    }

    public boolean deviceRemove(NetworkDataTypes.Auth auth, java.lang.String deviceName) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceRemove(auth, deviceName);
        return ret;
    }

    public boolean deviceRemoveCard(NetworkDataTypes.Auth auth, java.lang.String deviceName, java.lang.String hardwareAddress) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceRemoveCard(auth, deviceName, hardwareAddress);
        return ret;
    }

    public boolean deviceRemoveBulkInterface(NetworkDataTypes.Auth auth, java.lang.String deviceName, java.lang.String interfaceName) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceRemoveBulkInterface(auth, deviceName, interfaceName);
        return ret;
    }

    public boolean deviceMoveBulkInterface(NetworkDataTypes.Auth auth, java.lang.String deviceName, java.lang.String interfaceName, NetworkDataTypes.BulkInterface bulkInterface, NetworkDataTypes.BulkMoveOptions bulkMoveOptions) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceMoveBulkInterface(auth, deviceName, interfaceName, bulkInterface, bulkMoveOptions);
        return ret;
    }

    public boolean deviceUpdate(NetworkDataTypes.Auth auth, java.lang.String deviceName, NetworkDataTypes.DeviceInput deviceInput) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceUpdate(auth, deviceName, deviceInput);
        return ret;
    }

    public boolean deviceGlobalRename(NetworkDataTypes.Auth auth, java.lang.String deviceName, java.lang.String newDeviceName) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceGlobalRename(auth, deviceName, newDeviceName);
        return ret;
    }

    public boolean setHCPResponse(NetworkDataTypes.Auth auth, java.lang.String[] deviceList, boolean HCPFlag) throws java.rmi.RemoteException
    {
        boolean ret = impl.setHCPResponse(auth, deviceList, HCPFlag);
        return ret;
    }

    public boolean deviceUpdateIPv6Ready(NetworkDataTypes.Auth auth, java.lang.String deviceName, boolean IPv6Ready) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceUpdateIPv6Ready(auth, deviceName, IPv6Ready);
        return ret;
    }

    public boolean deviceUpdateManagerLock(NetworkDataTypes.Auth auth, java.lang.String deviceName, boolean managerLocked) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceUpdateManagerLock(auth, deviceName, managerLocked);
        return ret;
    }

    public boolean deviceSetBOOTPInfo(NetworkDataTypes.Auth auth, java.lang.String device, java.lang.String server, java.lang.String imagePath) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceSetBOOTPInfo(auth, device, server, imagePath);
        return ret;
    }

    public boolean deviceRemoveBOOTPInfo(NetworkDataTypes.Auth auth, java.lang.String device) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceRemoveBOOTPInfo(auth, device);
        return ret;
    }

    public NetworkDataTypes.BOOTPInfo getBOOTPInfo(NetworkDataTypes.Auth auth, java.lang.String device) throws java.rmi.RemoteException
    {
        NetworkDataTypes.BOOTPInfo ret = impl.getBOOTPInfo(auth, device);
        return ret;
    }

    public NetworkDataTypes.BulkInterface getBulkInterfaceInfo(NetworkDataTypes.Auth auth, java.lang.String interfaceName) throws java.rmi.RemoteException
    {
        NetworkDataTypes.BulkInterface ret = impl.getBulkInterfaceInfo(auth, interfaceName);
        return ret;
    }

    public boolean setInsertAddress(NetworkDataTypes.Auth auth, java.lang.String set, java.lang.String address) throws java.rmi.RemoteException
    {
        boolean ret = impl.setInsertAddress(auth, set, address);
        return ret;
    }

    public boolean setInsertService(NetworkDataTypes.Auth auth, java.lang.String set, java.lang.String service) throws java.rmi.RemoteException
    {
        boolean ret = impl.setInsertService(auth, set, service);
        return ret;
    }

    public boolean setDeleteAddress(NetworkDataTypes.Auth auth, java.lang.String set, java.lang.String address) throws java.rmi.RemoteException
    {
        boolean ret = impl.setDeleteAddress(auth, set, address);
        return ret;
    }

    public boolean setDeleteService(NetworkDataTypes.Auth auth, java.lang.String set, java.lang.String service) throws java.rmi.RemoteException
    {
        boolean ret = impl.setDeleteService(auth, set, service);
        return ret;
    }

    public NetworkDataTypes.SetInfo getSetInfo(NetworkDataTypes.Auth auth, java.lang.String setName) throws java.rmi.RemoteException
    {
        NetworkDataTypes.SetInfo ret = impl.getSetInfo(auth, setName);
        return ret;
    }

    public java.lang.String getSetNameFromID(NetworkDataTypes.Auth auth, long setID) throws java.rmi.RemoteException
    {
        java.lang.String ret = impl.getSetNameFromID(auth, setID);
        return ret;
    }

    public java.lang.String[] getSetAllInterfaces(NetworkDataTypes.Auth auth, java.lang.String setName) throws java.rmi.RemoteException
    {
        java.lang.String[] ret = impl.getSetAllInterfaces(auth, setName);
        return ret;
    }

    public java.lang.String[] getSetInterfacesTrusting(NetworkDataTypes.Auth auth, java.lang.String setName) throws java.rmi.RemoteException
    {
        java.lang.String[] ret = impl.getSetInterfacesTrusting(auth, setName);
        return ret;
    }

    public NetworkDataTypes.InetInfo[] getHCPInfoArray(NetworkDataTypes.Auth auth, java.lang.String[] hosts) throws java.rmi.RemoteException
    {
        NetworkDataTypes.InetInfo[] ret = impl.getHCPInfoArray(auth, hosts);
        return ret;
    }

    public java.lang.String[] getDevicesFromService(NetworkDataTypes.Auth auth, java.lang.String service) throws java.rmi.RemoteException
    {
        java.lang.String[] ret = impl.getDevicesFromService(auth, service);
        return ret;
    }

    public java.lang.String[] getSwitchesFromService(NetworkDataTypes.Auth auth, java.lang.String service) throws java.rmi.RemoteException
    {
        java.lang.String[] ret = impl.getSwitchesFromService(auth, service);
        return ret;
    }

    public NetworkDataTypes.SwitchPort[] getSwitchInfo(NetworkDataTypes.Auth auth, java.lang.String switchName) throws java.rmi.RemoteException
    {
        NetworkDataTypes.SwitchPort[] ret = impl.getSwitchInfo(auth, switchName);
        return ret;
    }

    public NetworkDataTypes.Connection[] getConnectionsFromDevice(NetworkDataTypes.Auth auth, java.lang.String deviceName) throws java.rmi.RemoteException
    {
        NetworkDataTypes.Connection[] ret = impl.getConnectionsFromDevice(auth, deviceName);
        return ret;
    }

    public NetworkDataTypes.OutletLocation getOutletLocationFromSwitchPort(NetworkDataTypes.Auth auth, java.lang.String switchName, java.lang.String portName) throws java.rmi.RemoteException
    {
        NetworkDataTypes.OutletLocation ret = impl.getOutletLocationFromSwitchPort(auth, switchName, portName);
        return ret;
    }

    public NetworkDataTypes.ObservedSwitchConnection[] getCurrentConnection(java.lang.String ip, java.lang.String[] hardwareAddressList) throws java.rmi.RemoteException
    {
        NetworkDataTypes.ObservedSwitchConnection[] ret = impl.getCurrentConnection(ip, hardwareAddressList);
        return ret;
    }

    public NetworkDataTypes.ObservedSwitchConnection[] getMyCurrentConnection(java.lang.String[] hardwareAddressList) throws java.rmi.RemoteException
    {
        NetworkDataTypes.ObservedSwitchConnection[] ret = impl.getMyCurrentConnection(hardwareAddressList);
        return ret;
    }

    public boolean enableFanOutFromSwitchPort(NetworkDataTypes.Auth auth, java.lang.String switchName, java.lang.String portName) throws java.rmi.RemoteException
    {
        boolean ret = impl.enableFanOutFromSwitchPort(auth, switchName, portName);
        return ret;
    }

    public boolean bindUnbindInterface(NetworkDataTypes.Auth auth, java.lang.String interfaceName, java.lang.String hardwareAddress) throws java.rmi.RemoteException
    {
        boolean ret = impl.bindUnbindInterface(auth, interfaceName, hardwareAddress);
        return ret;
    }

    public boolean interfaceAddAlias(NetworkDataTypes.Auth auth, java.lang.String interfaceName, java.lang.String alias) throws java.rmi.RemoteException
    {
        boolean ret = impl.interfaceAddAlias(auth, interfaceName, alias);
        return ret;
    }

    public boolean interfaceRemoveAlias(NetworkDataTypes.Auth auth, java.lang.String interfaceName, java.lang.String alias) throws java.rmi.RemoteException
    {
        boolean ret = impl.interfaceRemoveAlias(auth, interfaceName, alias);
        return ret;
    }

    public boolean interfaceMoveAlias(NetworkDataTypes.Auth auth, java.lang.String interfaceName, java.lang.String alias, java.lang.String newInterfaceName) throws java.rmi.RemoteException
    {
        boolean ret = impl.interfaceMoveAlias(auth, interfaceName, alias, newInterfaceName);
        return ret;
    }

    public boolean interfaceRename(NetworkDataTypes.Auth auth, java.lang.String interfaceName, java.lang.String newInterfaceName) throws java.rmi.RemoteException
    {
        boolean ret = impl.interfaceRename(auth, interfaceName, newInterfaceName);
        return ret;
    }

    public boolean interfaceMove(NetworkDataTypes.Auth auth, java.lang.String interfaceName, java.lang.String newDeviceName) throws java.rmi.RemoteException
    {
        boolean ret = impl.interfaceMove(auth, interfaceName, newDeviceName);
        return ret;
    }

    public java.lang.String[] searchSet(NetworkDataTypes.Auth auth, java.lang.String setPattern) throws java.rmi.RemoteException
    {
        java.lang.String[] ret = impl.searchSet(auth, setPattern);
        return ret;
    }

    public boolean setInsert(NetworkDataTypes.Auth auth, NetworkDataTypes.SetInput set) throws java.rmi.RemoteException
    {
        boolean ret = impl.setInsert(auth, set);
        return ret;
    }

    public boolean setRemove(NetworkDataTypes.Auth auth, java.lang.String setName) throws java.rmi.RemoteException
    {
        boolean ret = impl.setRemove(auth, setName);
        return ret;
    }

    public boolean setSwitchPortTypeStatus(NetworkDataTypes.Auth auth, java.lang.String switchName, java.lang.String portName, NetworkDataTypes.SwitchPortTypeStatus switchPortTypeStatus) throws java.rmi.RemoteException
    {
        boolean ret = impl.setSwitchPortTypeStatus(auth, switchName, portName, switchPortTypeStatus);
        return ret;
    }

    public boolean setSwitchPortService(NetworkDataTypes.Auth auth, java.lang.String switchName, java.lang.String portName, java.lang.String service) throws java.rmi.RemoteException
    {
        boolean ret = impl.setSwitchPortService(auth, switchName, portName, service);
        return ret;
    }

    public NetworkDataTypes.SwitchPortTypeStatus getSwitchPortTypeStatus(NetworkDataTypes.Auth auth, java.lang.String switchName, java.lang.String portName) throws java.rmi.RemoteException
    {
        NetworkDataTypes.SwitchPortTypeStatus ret = impl.getSwitchPortTypeStatus(auth, switchName, portName);
        return ret;
    }

    public NetworkDataTypes.NetNameTuple[] searchNetNameTable(NetworkDataTypes.Auth auth, java.lang.String netName) throws java.rmi.RemoteException
    {
        NetworkDataTypes.NetNameTuple[] ret = impl.searchNetNameTable(auth, netName);
        return ret;
    }

    public boolean deviceAddLogicalInterface(NetworkDataTypes.Auth auth, java.lang.String deviceName, NetworkDataTypes.LogicalInterfaceInput logicalInterface) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceAddLogicalInterface(auth, deviceName, logicalInterface);
        return ret;
    }

    public boolean deviceRemoveLogicalInterface(NetworkDataTypes.Auth auth, java.lang.String deviceName, java.lang.String interfaceName) throws java.rmi.RemoteException
    {
        boolean ret = impl.deviceRemoveLogicalInterface(auth, deviceName, interfaceName);
        return ret;
    }

    public boolean interfaceUpdateDescription(NetworkDataTypes.Auth auth, java.lang.String interfaceName, java.lang.String description) throws java.rmi.RemoteException
    {
        boolean ret = impl.interfaceUpdateDescription(auth, interfaceName, description);
        return ret;
    }

    public boolean serviceUpdateDescription(NetworkDataTypes.Auth auth, java.lang.String serviceName, java.lang.String description) throws java.rmi.RemoteException
    {
        boolean ret = impl.serviceUpdateDescription(auth, serviceName, description);
        return ret;
    }

    public NetworkDataTypes.ServiceInfo getServiceInfo(NetworkDataTypes.Auth auth, java.lang.String serviceName) throws java.rmi.RemoteException
    {
        NetworkDataTypes.ServiceInfo ret = impl.getServiceInfo(auth, serviceName);
        return ret;
    }

    public boolean vmCreate(NetworkDataTypes.Auth auth, NetworkDataTypes.DeviceInput VMDevice, NetworkDataTypes.VMCreateOptions VMCreateOptions) throws java.rmi.RemoteException
    {
        boolean ret = impl.vmCreate(auth, VMDevice, VMCreateOptions);
        return ret;
    }

    public boolean vmMigrate(NetworkDataTypes.Auth auth, java.lang.String VMName, java.lang.String newParent) throws java.rmi.RemoteException
    {
        boolean ret = impl.vmMigrate(auth, VMName, newParent);
        return ret;
    }

    public boolean vmUpdate(NetworkDataTypes.Auth auth, java.lang.String deviceName, NetworkDataTypes.DeviceInput deviceInput) throws java.rmi.RemoteException
    {
        boolean ret = impl.vmUpdate(auth, deviceName, deviceInput);
        return ret;
    }

    public boolean vmDestroy(NetworkDataTypes.Auth auth, java.lang.String VMName) throws java.rmi.RemoteException
    {
        boolean ret = impl.vmDestroy(auth, VMName);
        return ret;
    }

    public NetworkDataTypes.VMClusterInfo vmClusterGetInfo(NetworkDataTypes.Auth auth, java.lang.String VMClusterName) throws java.rmi.RemoteException
    {
        NetworkDataTypes.VMClusterInfo ret = impl.vmClusterGetInfo(auth, VMClusterName);
        return ret;
    }

    public java.lang.String[] vmClusterGetDevices(NetworkDataTypes.Auth auth, java.lang.String VMClusterName) throws java.rmi.RemoteException
    {
        java.lang.String[] ret = impl.vmClusterGetDevices(auth, VMClusterName);
        return ret;
    }

    public NetworkDataTypes.VMInfo vmGetInfo(NetworkDataTypes.Auth auth, java.lang.String VMName) throws java.rmi.RemoteException
    {
        NetworkDataTypes.VMInfo ret = impl.vmGetInfo(auth, VMName);
        return ret;
    }

    public java.lang.String[] vmGetClusterMembership(NetworkDataTypes.Auth auth, java.lang.String deviceName) throws java.rmi.RemoteException
    {
        java.lang.String[] ret = impl.vmGetClusterMembership(auth, deviceName);
        return ret;
    }

    public java.lang.String[] vmSearchCluster(NetworkDataTypes.Auth auth, NetworkDataTypes.VMClusterSearch VMClusterSearch) throws java.rmi.RemoteException
    {
        java.lang.String[] ret = impl.vmSearchCluster(auth, VMClusterSearch);
        return ret;
    }

    public boolean vmAddInterface(NetworkDataTypes.Auth auth, java.lang.String VMName, java.lang.String interfaceName, java.lang.String VMClusterName, NetworkDataTypes.VMInterfaceOptions VMInterfaceOptions) throws java.rmi.RemoteException
    {
        boolean ret = impl.vmAddInterface(auth, VMName, interfaceName, VMClusterName, VMInterfaceOptions);
        return ret;
    }

    public boolean vmRemoveInterface(NetworkDataTypes.Auth auth, java.lang.String VMName, java.lang.String interfaceName) throws java.rmi.RemoteException
    {
        boolean ret = impl.vmRemoveInterface(auth, VMName, interfaceName);
        return ret;
    }

    public boolean vmMoveInterface(NetworkDataTypes.Auth auth, java.lang.String VMName, java.lang.String interfaceName, java.lang.String VMClusterName, NetworkDataTypes.VMInterfaceOptions VMInterfaceOptions) throws java.rmi.RemoteException
    {
        boolean ret = impl.vmMoveInterface(auth, VMName, interfaceName, VMClusterName, VMInterfaceOptions);
        return ret;
    }

    public java.lang.String vmAddCard(NetworkDataTypes.Auth auth, java.lang.String VMName, NetworkDataTypes.InterfaceCard interfaceCard) throws java.rmi.RemoteException
    {
        java.lang.String ret = impl.vmAddCard(auth, VMName, interfaceCard);
        return ret;
    }

    public boolean vmRemoveCard(NetworkDataTypes.Auth auth, java.lang.String VMName, java.lang.String hardwareAddress) throws java.rmi.RemoteException
    {
        boolean ret = impl.vmRemoveCard(auth, VMName, hardwareAddress);
        return ret;
    }

    public boolean dnsZoneUpdate(NetworkDataTypes.Auth auth, java.lang.String zone, NetworkDataTypes.DnsZoneOptions dnsZoneOptions) throws java.rmi.RemoteException
    {
        boolean ret = impl.dnsZoneUpdate(auth, zone, dnsZoneOptions);
        return ret;
    }

    public NetworkDataTypes.DNSDelegatedEntry[] dnsDelegatedSearch(NetworkDataTypes.Auth auth, java.lang.String search) throws java.rmi.RemoteException
    {
        NetworkDataTypes.DNSDelegatedEntry[] ret = impl.dnsDelegatedSearch(auth, search);
        return ret;
    }

    public NetworkDataTypes.DNSDelegatedEntry dnsDelegatedGetByNameView(NetworkDataTypes.Auth auth, java.lang.String domain, java.lang.String view) throws java.rmi.RemoteException
    {
        NetworkDataTypes.DNSDelegatedEntry ret = impl.dnsDelegatedGetByNameView(auth, domain, view);
        return ret;
    }

    public boolean dnsDelegatedAdd(NetworkDataTypes.Auth auth, NetworkDataTypes.DNSDelegatedInput DNSDelegatedInput) throws java.rmi.RemoteException
    {
        boolean ret = impl.dnsDelegatedAdd(auth, DNSDelegatedInput);
        return ret;
    }

    public NetworkDataTypes.DNSDelegatedKey[] dnsDelegatedListKeys(NetworkDataTypes.Auth auth) throws java.rmi.RemoteException
    {
        NetworkDataTypes.DNSDelegatedKey[] ret = impl.dnsDelegatedListKeys(auth);
        return ret;
    }

    public boolean dnsDelegatedRemove(NetworkDataTypes.Auth auth, java.lang.String domain, java.lang.String view) throws java.rmi.RemoteException
    {
        boolean ret = impl.dnsDelegatedRemove(auth, domain, view);
        return ret;
    }

    public boolean dnsDelegatedAliasAdd(NetworkDataTypes.Auth auth, java.lang.String domain, java.lang.String view, java.lang.String alias) throws java.rmi.RemoteException
    {
        boolean ret = impl.dnsDelegatedAliasAdd(auth, domain, view, alias);
        return ret;
    }

    public boolean dnsDelegatedAliasRemove(NetworkDataTypes.Auth auth, java.lang.String domain, java.lang.String view, java.lang.String alias) throws java.rmi.RemoteException
    {
        boolean ret = impl.dnsDelegatedAliasRemove(auth, domain, view, alias);
        return ret;
    }

}
